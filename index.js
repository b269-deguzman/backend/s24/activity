
const getCube = 3**3;
const num = 5;
const message = `The cube of ${num} is ${getCube}`;
console.log(message);

const address = [258, 'Washington Ave NW', 'California', 90011]

const [streetNum, streetName, state, zipCode] = address;
const secondMessage = `I live at ${streetNum} ${streetName}, ${state} ${zipCode}`;
console.log(secondMessage);

const animal = {
	animalName: "Lolong",
	animalType: 'saltwater crocodile',
	animalWeight: 1075,
	feet: 20,
	inch: 3
	}

const {animalName, animalType, animalWeight, feet, inch}= animal;

const thirdMessage = `${animalName} was a ${animalType}. He weighed at ${animalWeight} kgs with a measurement of ${feet} ft ${3} in.`
console.log(thirdMessage);

const numbers = [10,20,30,40,50];
numbers.forEach((number)=>{
	console.log(number);
})


const reduceNumber = numbers.reduce((previousNum, currentNum)=>{
	sum = previousNum + currentNum;
	return sum
})
console.log(reduceNumber);


class Dog{
	constructor(name, age, breed){
		this.name= name;
		this.age = age; 
		this.breed = breed;
	}
}

const myDog = new Dog('Frankie', 5, "Miniature Dachshund");
console.log(myDog);

